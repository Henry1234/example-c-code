# example c code

This repository contains the Atmel Studio C project "DataLogger".

/*
 * DataLogger.c
 *
 * This example gets or sets port values via commands from UART. 
 * UART is using 9600 BAUD, 8 databits, 1 stop bit, no parity.
 *
 * it reads temperature from from a TMP36 via the ADC0 (PortC0).
 * It detects button presses on PORTB pin 0 and 7
 * It manipulates led's connected to PORTB pin 4 and 5
 *
 * UART commands:
 *
 * led1on (turns on led 1, replies with led1=0 or led1=1)
 * led2on (turns on led 2, replies with led2=0 or led2=1)
 * getbtn1 (reads button 1 status, replies with btn1=0 or btn=1, resets status if btn1=1)
 * getbtn2 (reads button 1 status, replies with btn1=0 or btn=2, resets status if btn2=1)
 * gettemp (returns temperature in celcius from TMP36)
 * getadcval (returns raw adc value from TMP36)
 *
 * Commands not recognized gets a response from HAL9000
 *
 * Created: 14-01-2019 12:44:50
 * Author : Nikolaj Simonsen
 *
 */ 

